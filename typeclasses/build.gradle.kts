import Versions.arrowVersion
import Versions.junitVersion
import Versions.spek2Version

plugins {
    id("common-deps")
    idea
    `maven-publish`
}

dependencies {
    implementation(project(":models"))
}


/**
 * Setup generated sources to be identified by idea as well as build process
 * Refer https://blog.nishtahir.com/2016/06/25/how-to-get-idea-to-detect-kotlin-generated-sources-using-gradle/
 */
sourceSets["main"].java.srcDir(file("build/generated/source/kaptKotlin/main/extension"))
idea {
    module {
        generatedSourceDirs.add(
            file("build/generated/source/kaptKotlin/main/extension")
        )
    }
}
/** ------ */
