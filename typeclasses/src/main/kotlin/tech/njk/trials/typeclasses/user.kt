package tech.njk.trials.typeclasses

import arrow.extension
import arrow.typeclasses.Eq
import tech.njk.trials.models.User

@extension
interface UserEq: Eq<User> {
    override fun User.eqv(b: User): Boolean = this.id == b.id
}
