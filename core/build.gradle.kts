plugins {
    id("common-deps")
    application
}

application {
    mainClassName = "tech.njk.trials.core.MainKt"
}

dependencies {
    implementation(project(":typeclasses"))
    implementation(project(":models"))
    compile("joda-time", "joda-time", "2.10.1")
}
