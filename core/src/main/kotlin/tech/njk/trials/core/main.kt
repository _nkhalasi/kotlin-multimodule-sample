package tech.njk.trials.core

import tech.njk.trials.models.User
import tech.njk.trials.typeclasses.user.eq.eq

fun main() {
    val x = User.eq().run {
        User(1).eqv(User(2))
    }
    println(x)
}