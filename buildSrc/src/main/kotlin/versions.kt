object Versions {
    const val arrowVersion = "0.9.0"
    const val spek2Version = "2.0.0"
    const val junitVersion = "5.2.0"
}
