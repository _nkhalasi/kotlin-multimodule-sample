import Versions.junitVersion
import Versions.spek2Version
import Versions.arrowVersion

plugins {
    kotlin("jvm")
    kotlin("kapt")
}

dependencies {
    "runtime"(kotlin("reflect"))
    "compile"("ch.qos.logback", "logback-classic", "1.3.0-alpha4")
    "compile"("io.arrow-kt", "arrow-core-data", arrowVersion)
    "compile"("io.arrow-kt", "arrow-core-extensions", arrowVersion)
    "compile"("io.arrow-kt", "arrow-syntax", arrowVersion)
    "compile"("io.arrow-kt", "arrow-typeclasses", arrowVersion)
    "compile"("io.arrow-kt", "arrow-extras-data", arrowVersion)
    "compile"("io.arrow-kt", "arrow-extras-extensions", arrowVersion)
    "compile"("io.arrow-kt", "arrow-effects-extensions", arrowVersion)
    "compile"("io.arrow-kt", "arrow-effects-io-extensions", arrowVersion)
    "compile"("io.arrow-kt", "arrow-effects-rx2-data", arrowVersion)
    "compile"("io.arrow-kt", "arrow-effects-rx2-extensions", arrowVersion)
    "compile"("org.jetbrains.kotlinx", "kotlinx-coroutines-core", "1.1.1")

    "kapt"("io.arrow-kt", "arrow-meta", arrowVersion)

    "testImplementation"("org.junit.jupiter", "junit-jupiter-api", junitVersion)
    "testImplementation"("org.spekframework.spek2", "spek-dsl-jvm", spek2Version)
        .exclude("org.jetbrains.kotlin")
    "testImplementation"("org.amshove.kluent", "kluent", "1.47")
        .exclude("junit")
    "testRuntimeOnly"("org.junit.jupiter", "junit-jupiter-engine", junitVersion)
    "testRuntimeOnly"("org.spekframework.spek2", "spek-runner-junit5", spek2Version)
        .exclude("org.junit.platform")
        .exclude("org.jetbrains.kotlin")
}