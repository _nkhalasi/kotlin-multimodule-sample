import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    base
    kotlin("jvm") version "1.3.21" apply false
    kotlin("kapt") version "1.3.21" apply false
}

allprojects {
    group = "tech.njk.trials"
    version = "1.0-SNAPSHOT"

    repositories {
        mavenCentral()
        jcenter()
//        maven(url = "https://oss.jfrog.org/artifactory/oss-snapshot-local/")
        mavenLocal()
    }

    tasks.withType<KotlinCompile> {
        kotlinOptions.jvmTarget = "1.8"
    }

}

//dependencies {
//    subprojects.forEach {
//        archives(it)
//    }
//}
